Ansible Collection - havlasme.database
======================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) collection of database-related roles, plugins, and modules:

- [havlasme.database.mariadb](/roles/mariadb/README.md)
- [havlasme.database.memcached](/roles/memcached/README.md)

Installation
------------

```bash
ansible-galaxy collection install havlasme.database
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: LICENSE
