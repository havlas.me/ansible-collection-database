def expand_dest(dest, basepath):
    return dest if dest.startswith('/') else basepath + '/' + dest

class FilterModule(object):
    def filters(self):
        return {'expand_dest': expand_dest}
